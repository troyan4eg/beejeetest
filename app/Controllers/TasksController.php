<?php

namespace App\Controllers;


use App\Task;

class TasksController extends BaseController
{
    public function index()
    {
        $template = $this->twig->load("index.html.twig");
        echo $template->render([]);
    }

    public function create()
    {
        $creator = !empty($_POST["creator"]) ? $_POST["creator"] : null;
        $email = !empty($_POST["email"]) ? $_POST["email"] : null;
        $text = !empty($_POST["task"]) ? $_POST["task"] : null;

        $task = new Task();
        $task->creator = $creator;
        $task->email = $email;
        $task->text = htmlentities($text);
        $task->done = 0;

        $errors = $task->validate();
        if (!empty($errors)) {
            $this->redirect();
        }

        $task->Add();
        $this->redirect();
    }
}