<?php

namespace App\Controllers;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class BaseController
{
    protected $twig_loader;
    protected $twig;
    protected $errors = [];

    public function __construct()
    {
        $this->twig_loader = new FilesystemLoader(BASE_DIR . '/views');
        $this->twig = new Environment($this->twig_loader, [
//            'cache' => BASE_DIR . '/cache',
            'debug' => true
        ]);
        $this->twig->addExtension(new \Twig\Extension\DebugExtension());
        $this->twig->addGlobal('login_user_id', !empty($_SESSION["LOGIN_USER_ID"]) ? $_SESSION["LOGIN_USER_ID"] : null);
        $this->twig->addGlobal('errors', !empty($this->errors) ? $this->errors : []);
    }

    function checkAuth() {
        if (empty($_SESSION["LOGIN_USER_ID"])) {
            echo json_encode(['success' => false, 'errors' => ["Время жизни сессии истекло, пожалуйста авторизуйтесь заново"]]);
            exit();
        }
        return true;
    }

    function redirect($route = '') {
        header('Location: http://' . $_SERVER['HTTP_HOST'] . '/' . $route);
        exit();
    }
}