<?php


namespace App\Controllers\Auth;


use App\Controllers\BaseController;
use App\User;

class AuthController extends BaseController
{
    public function login()
    {
        $username = !empty($_POST["username"]) ? $_POST["username"] : null;
        $password = !empty($_POST["password"]) ? $_POST["password"] : null;

        if (empty($username)) {
            $this->errors[] = "Имя пользователя не может быть пустым!";
        }

        if (empty($password)) {
            $this->errors[] = "Пароль не может быть пустым!";
        }

        if (!empty($this->errors)) {
           $this->redirect();
        }

        $users = User::select("SELECT * FROM `users` WHERE `name` = '$username' AND password='" . md5($password) . "' LIMIT 1");
        if (empty($users)) {
            $template = $this->twig->load("index.html.twig");
            echo $template->render(['errors' => "Неправельное имя пользователя или пароль"]);
            exit();
        }

        $_SESSION["LOGIN_USER_ID"] =  $users[0]->id;

        $this->redirect();
    }

    public function logout()
    {
        unset($_SESSION["LOGIN_USER_ID"]);
        $this->redirect();
    }
}