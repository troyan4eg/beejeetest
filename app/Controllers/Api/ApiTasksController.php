<?php

namespace App\Controllers\Api;


use App\Controllers\BaseController;
use App\Task;

class ApiTasksController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
//        if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')
//        {
//            header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
//            exit();
//        }
    }

    public function tasksList()
    {
        $start = !empty($_GET["start"]) && $_GET["start"] > 0 ? $_GET["start"] : 0;
        $length = !empty($_GET["length"]) && $_GET["length"] > 0 ? $_GET["length"] : 0;
        $orderBy = null;
        $order = 'asc';
        if (!empty($_GET["order"]) && is_array($_GET["order"])) {
            $columns = ["creator", "email", "text", "done"];
            if (in_array($_GET["order"][0]['column'], array_keys($columns))) {
                $orderBy = $columns[$_GET["order"][0]['column']];
                $order = $_GET["order"][0]['dir'];
            }
        }
//        print_r([$length, $start, $orderBy, $order]);
        $tasks = Task::all($length, $start, $orderBy, $order);
        $total = Task::total();
        echo json_encode(["data" => array_values($tasks), "recordsTotal" => $total, "recordsFiltered" => $total]);
    }

    public function update($params)
    {
        $this->checkAuth();
        parse_str(file_get_contents("php://input"),$post_vars);
        if (empty($params['id'])) {
            echo json_encode(['success' => false, 'errors' => ['Задача не найдена']]);
            exit();
        }
        /** @var Task $task */
        $task = Task::findID($params['id']);
        if (empty($task)) {
            echo json_encode(['success' => false, 'errors' => ['Задача не найдена']]);
            exit();
        }
        if(empty($post_vars['text'])) {
            echo json_encode(['success' => false, 'errors' => ['Текст задачи не может быть пустым']]);
            exit();
        }
        $task->text = htmlentities($post_vars['text']);
        $task->note .= (!empty($task->note) ? ', ' : '') ."отредактировано администратором";
        $task->Save();
        echo json_encode(['success' => true, 'text' => $task->text]);
    }

    public function check($params)
    {
        $this->checkAuth();
        if (empty($params['id'])) {
            echo json_encode(['success' => false, 'errors' => ['Задача не найдена']]);
            exit();
        }
        /** @var Task $task */
        $task = Task::findID($params['id']);
        if (empty($task)) {
            echo json_encode(['success' => false, 'errors' => ['Задача не найдена']]);
            exit();
        }
        if (!$task->done) {
            $task->done = true;
            $task->note .= (!empty($task->note) ? ', ' : '') ."выполнена";
            $task->Save();
        }
        echo json_encode(['success' => true]);
    }
}