<?php

namespace App;

use mysqli;
use SplStack;

class Model
{
    const TABLE = null;

    /** @var mysqli $db */
    private static $db;
    private static $stack;

    /**
     * @param mysqli $dbi
     * @param string $enc
     * @return
     * @throws \Exception
     */
    static function setup(mysqli $dbi, $enc = "utf8")
    {
        if (is_object($dbi)) {
            self::$db = $dbi;
            self::$stack = new SplStack();
            return self::setEncoding($enc);
        } else {
            throw new \Exception("Param $dbi is not mysqli object", 1);
        }
    }

    static function setEncoding($enc)
    {
        $result = self::$db->query("SET NAMES '$enc'");
        self::$stack->push("SET NAMES '$enc' [" . ($result ? "TRUE" : self::getError()) . "]");
        return $result ? true : false;
    }

    static function getError()
    {
        return self::$db->error . " [" . self::$db->errno . "]";
    }

    private static function escape($string)
    {
        return mysqli_real_escape_string(self::$db, $string);
    }

    private static function _getVars()
    {
        return array_filter(get_class_vars(get_called_class()), function ($elem) {
            return (!is_object($elem));
        });
    }

    static function findID($id)
    {
        if (is_numeric($id)) {
            $query = "SELECT * FROM `" . get_called_class()::TABLE . "` WHERE `" . key(self::_getVars()) . "` = $id LIMIT 1";
            $result = self::$db->query($query);
            self::$stack->push($query . " [" . $result->num_rows . "]");
            if ($result->num_rows == 1) {
                $row = $result->fetch_object();
                $cName = get_called_class();
                $rClass = new $cName();
                foreach ($row as $key => $value) $rClass->$key = $value;
                return $rClass;
            } else return false;
        } else return false;
    }

    static function select($query)
    {
        $result = self::$db->query($query);
        self::$stack->push($query . " [" . $result->num_rows . "]");
        if (!$result->num_rows) {
            return [];
        }
        $data = $result->fetch_all(MYSQLI_ASSOC);
        $_res = [];
        foreach ($data as $datum) {
            $cName = get_called_class();
            $rClass = new $cName();
            foreach ($datum as $key => $value) {
                $rClass->$key = $value;
            }
            $_res[] = $rClass;
        }
        return $_res;
    }

    static function all($limit = 0, $offset = 0, $orderBy = null, $order = 'desc')
    {
        $query = "SELECT * FROM `" . get_called_class()::TABLE ."`";
        if(!empty($orderBy)) {
            $query .= " ORDER BY $orderBy $order";
        }
        if($limit) {
            $query .= " LIMIT $limit OFFSET $offset";
        }
        return self::select($query);
    }

    static function total() {
        $query = "SELECT COUNT(*) as `row_count` FROM `" . get_called_class()::TABLE ."`";
        $result = self::$db->query($query);
        self::$stack->push($query . " [" . $result->num_rows . "]");
        $row = $result->fetch_object();
        return $row->row_count;
    }

    public function Save()
    {
        $id = key(self::_getVars());
        if (!isset($this->$id) || empty($this->$id)) return $this->Add();
        $query = "UPDATE `" . get_called_class()::TABLE . "` SET ";
        $columns = self::_getVars();
        $Update = array();
        foreach ($columns as $k => $v) {
            if ($id != $k)
                $Update[] = "`" . $k . "` = " . self::RenderField($this->$k);
        }
        $query .= join(", ", $Update);
        $query .= " WHERE `$id` = " . self::escape($this->$id) . " LIMIT 1";
        $result = self::$db->query($query);
        self::$stack->push($query . " [" . ($result ? "TRUE" : self::getError()) . "]");
        return ($result) ? true : false;
    }

    private static function RenderField($field)
    {
        switch (gettype($field)) {
            case "integer":
            case "float":
                $r = $field;
                break;
            case "NULL":
                $r = "NULL";
                break;
            case "boolean":
                $r = ($field) ? "true" : "false";
                break;
            case "string":
                $p_function = "/^[a-zA-Z_]+\((.)*\)/";
                preg_match($p_function, $field, $mathes);
                if (isset($mathes[0])) {
                    $p_value = "/\((.+)\)/";
                    preg_match($p_value, $field, $mValue);
                    if (isset($mValue[0]) && !empty($mValue[0])) {
                        $pv = trim($mValue[0], "()");
                        $pv = "'" . self::escape($pv) . "'";
                        $r = preg_replace($p_value, "($pv)", $field);
                    } else $r = $field;
                } else $r = "'" . self::escape($field) . "'";
                break;
            default:
                $r = "'" . self::escape($field) . "'";
                break;
        }
        return $r;
    }

    public function Add()
    {
        $query = "INSERT INTO `" . get_called_class()::TABLE . "` (";
        $columns = self::_getVars();
        $q_column = array();
        $q_data = array();
        foreach ($columns as $k => $v) {
            $q_column[] = "`" . $k . "`";
            $q_data[] = self::RenderField($this->$k);
        }
        $query .= join(", ", $q_column) . ") VALUES (";
        $query .= join(", ", $q_data) . ")";
        $result = self::$db->query($query);
        $insert_id = self::$db->insert_id;
        self::$stack->push($query . " [" . ($result ? $insert_id : self::getError()) . "]");
        return ($result) ? $insert_id : false;
    }

    public function Remove()
    {
        $id = key(self::_getVars());
        if (!empty($this->$id)) {
            $qDel = "DELETE FROM `" . get_called_class()::TABLE . "` WHERE `$id` = " . $this->$id . " LIMIT 1";
            $rDel = self::$db->query($qDel);
            self::$stack->push($qDel . " [" . ($rDel ? "TRUE" : self::getError()) . "]");
            return $rDel ? true : false;
        } else return false;
    }
}