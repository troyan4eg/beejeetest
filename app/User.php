<?php


namespace App;

class User extends Model
{
    const TABLE = 'users';

    public $id;
    public $name;
    public $email;
}