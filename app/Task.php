<?php


namespace App;

class Task extends Model
{
    const TABLE = 'tasks';

    public $id;
    public $creator;
    public $email;
    public $text;
    public $done;
    public $note;

    public function validate()
    {
        $errors = [];
        if (empty($this->creator)) {
            $errors[] = "Creator is Required!";
        }
        if (empty($this->email)) {
            $errors[] = "Email is Required!";
        }
        if (empty($this->text)) {
            $errors[] = "Text is Required!";
        }
        return $errors;
    }
}