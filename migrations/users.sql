create table users
(
    id       int auto_increment
        primary key,
    name     varchar(100) not null,
    email    varchar(100) not null,
    password varchar(100) not null,
    constraint users_email_uindex
        unique (email)
);

INSERT INTO `users`
(`name`, `email`, `password`)
VALUES ("admin", "admin@email.loc", "202cb962ac59075b964b07152d234b70")