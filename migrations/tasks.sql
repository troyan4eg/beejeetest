create table tasks
(
	id int auto_increment,
	user_id int default null null,
	creator varchar(100) not null,
	email varchar(100) not null,
	text text not null,
	done tinyint default 0 not null,
	note varchar(150) default null
	constraint tasks_pk
		primary key (id)
);

