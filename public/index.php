<?php

use App\Lib\AltoRouter;

if (!is_file(__DIR__ . '/../vendor/autoload.php')) {
    echo "No `vendor` directory. Please run `composer install` command, before use site";
    die;
}
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config/init.php';

$uri = $_SERVER['REQUEST_URI'];
$api_pos = mb_strpos($uri, '/');
$folder = mb_substr($uri, 1, $api_pos - 1);
$uri = mb_substr($uri, $api_pos, strlen($uri));

$uri = explode('/', $uri);

if (empty($uri[1])) {
    $uri[1] = '/';
}

$router = new AltoRouter();

$router->map( 'GET', '/', 'App\Controllers\TasksController@index');
$router->map( 'POST', '/tasks', 'App\Controllers\TasksController@create');
$router->map( 'POST', '/login', 'App\Controllers\Auth\AuthController@login' );
$router->map( 'GET', '/logout', 'App\Controllers\Auth\AuthController@logout' );

// Api
$router->map( 'GET', '/tasks', 'App\Controllers\Api\ApiTasksController@tasksList');
$router->map( 'PUT', '/tasks/[i:id]', 'App\Controllers\Api\ApiTasksController@update');
$router->map( 'PUT', '/tasks/[i:id]/check', 'App\Controllers\Api\ApiTasksController@check');

$match = $router->match();

if ($match) {
    $methodParams = $match ? explode('@', $match['target']) : [];
    $methodParams[0] = new $methodParams[0];
    if(is_callable($methodParams)) {
        call_user_func($methodParams, $match["params"]);
    }
} else {
    header( $_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    exit();
}