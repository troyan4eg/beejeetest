let loggedIn = false;
if ($('#loggedIn').length && +$('#loggedIn').val() === 1) {
    loggedIn = true;
}

changeMode = false;

if ($('#tasks_table').length) {
    $('#tasks_table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "/tasks",
        "bLengthChange": false,
        "searching": false,
        "pageLength": 3,
        columns: [
            {data: 'creator'},
            {data: 'email'},
            {
                data: "text",
                searchable: false,
                sortable: true,
                visible: true,
                className: "text-center",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<div style="min-width: 50px; min-height: 15px" id="task-' + row.id + '" data-id="' + row.id + '" onclick="changeText(this)">' + data.toString() + '</div>';
                    }
                    return data;
                }

            },
            {
                data: "done",
                searchable: false,
                sortable: true,
                visible: true,
                className: "text-center",
                render: function (data, type, row) {
                    if (type === 'display') {
                        return '<input type="checkbox" data-id="' + row.id + '" onclick="check(this)" class="editor-active task-done" ' + (data == 1 ? 'checked ' : '') + (!loggedIn || data == 1 ? 'disabled' : '') + '>';
                    }
                    return data;
                }

            },
            {data: 'note'},
        ],
        columnDefs: [
            {targets: 'no-sort', orderable: false}
        ]
    });
}

function changeText(el) {
    if (!changeMode && loggedIn) {
        el = $(el);
        const id = el.data('id');
        el.html('<textarea id="text-' + id + '">' + el.text() + '</textarea><button data-id="' + id + '" class="btn btn-success" onclick="updateText(this)">Save</button>')
        changeMode = true;
    }
}

function updateText(el) {
    el = $(el);
    const id = el.data('id');
    const text = $('#text-' + id).val();
    $.ajax({
        url: '/tasks/' + id,
        type: 'PUT',
        data: {text},
        success: (res) => {
            const result = JSON.parse(res);
            changeMode = false;
            if (!result.success) {
                Swal.fire({
                    icon: 'error',
                    title: 'Не удалось обновить задачу',
                    text: result.errors[0]
                })
            } else {
                $('#task-' + id).html(result.text)
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Задача успешно обновлена',
                    showConfirmButton: false,
                    timer: 1500
                }).then((result) => {
                    window.location.reload()
                })
            }
        }
    });
}

function check(el) {
    const chechbox = $(el);
    const id = chechbox.data('id');
    $.ajax({
        url: '/tasks/' + id + '/check',
        type: 'PUT',
        success: (res) => {
            const result = JSON.parse(res);
            if (!result.success) {
                chechbox.prop('checked', false);
                Swal.fire({
                    icon: 'error',
                    title: 'Не удалось обновить статус',
                    text: result.errors[0]
                })
            } else {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Статус успешно обновлён',
                    showConfirmButton: false,
                    timer: 1500
                }).then((result) => {
                    window.location.reload()
                })
            }
        }
    });
}