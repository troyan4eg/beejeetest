<?php

use App\Model;

if(!is_file( __DIR__ . '/config.php')) {
    echo "Please Define /config/config.php. Copy from  /config/config.sample.php and change params";
    die;
}
require_once __DIR__ . '/config.php';

$db_connect = mysqli_connect(DB_CONFIG["host"], DB_CONFIG["user"], DB_CONFIG["password"], DB_CONFIG["name"]);
Model::setup($db_connect);

ob_start();
session_start();